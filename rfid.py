# Mqtt Reader. Read RFID tags and broadcast UID via MQTT. This is educational software.
# Copyright (C) 2019 Marcus Gelderie
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import contextlib
import logging
import mfrc522

logger = logging.getLogger(__name__)


def _trailer_block_for_sector(sector_number):
    if sector_number < 0 or sector_number >= 16:
        raise ValueError('Sector number out of range')
    trailer = sector_number * 4 + 3
    if not trailer % 4 == 3:
        logger.error('Computed trailer number was illegal: %s', trailer)
        raise AssertionError('Trailer number was illegal.')
    return trailer


class Token:
    def __init__(self, uid, card_type, reader):
        self._uid = uid
        self._reader = reader
        self._card_type = card_type

    def uid(self):
        return self._uid

    def read(self, sector):
        data = self._reader.read_from_card(self._uid, sector)
        if data is None:
            logger.error('Error reading from card %s', self._uid)
            return None
        logger.debug('Got %s bytes of data.', len(data))
        return data

    def __str__(self):
        return ':'.join(str(i) for i in self._uid)

    def write(self, sector, data):
        if len(data) > 3 * 16:
            raise ValueError('Received too much data for one sector')
        if len(data) < 3 * 16:
            logger.info('Padding short data with 0x00')
            data.extend(bytes(16 * 3 - len(data)))
        if self._reader.write_to_card(self._uid, sector, data) is None:
            logger.error('Could not write to card.')


class Reader:
    def __init__(self, key=[0xff for i in range(6)]):
        if len(key) != 6:
            logger.error('Received key of size %s', len(key))
            raise ValueError('Key must have 6 bytes')
        self._key = key
        self._reader = mfrc522.MFRC522()

    def get_card(self):
        status, tag_type = self._reader.MFRC522_Request(
            self._reader.PICC_REQIDL)
        if status != self._reader.MI_OK:
            return None
        logger.debug('Found a card in vincinity')
        status, uid = self._reader.MFRC522_Anticoll()
        if status != self._reader.MI_OK:
            logger.debug('Could not perform anticollision protocol')
            return None
        token = Token(uid, tag_type, self)
        logger.info('Got card %s', token)
        return token

    def read_from_card(self, uid, sector_number, num_retries=3):
        with self._select_tag_and_athenticate(uid, sector_number) as success:
            if not success:
                logger.error(
                    'Could not select or authenticate card with UID %s', uid)
                return None
            data = []
            for i in range(3):
                for _ in range(num_retries):
                    block = self._reader.MFRC522_Read(sector_number * 4 + i)
                    if block:
                        break
                if not block:
                    logger.error('Could not read block %d of sector %d', i,
                                 sector_number)
                    return None
                data += block
            if not data:
                logger.warning('Did not read anything from card')
            return data

    @contextlib.contextmanager
    def _select_tag_and_athenticate(self, uid, sector_number):
        logger.debug('Selecting card with UID %s', uid)
        received_data = self._reader.MFRC522_SelectTag(uid)
        if received_data == 0:
            logger.warning('Received error when selecting tag with UID %s',
                           uid)
            logger.warning('Attempting reconnect...')
            tcard = self.get_card()
            if not tcard or tcard.uid() != uid:
                logger.error(
                    'Cannot find card anymore. Found %s, but was looking for %s.',
                    tcard, uid)
                yield False
                return
            logger.info('Found card on second attempt. Selecting...')
            received_data = self._reader.MFRC522_SelectTag(uid)
            if received_data == 0:
                logger.error(
                    'Again received error when selecting tag with UID %s', uid)
                yield False
                return
        status = self._reader.MFRC522_Auth(
            self._reader.PICC_AUTHENT1A,
            _trailer_block_for_sector(sector_number), self._key, uid)
        if status != self._reader.MI_OK:
            logger.error(
                'Could not authenticate token with given key. UID = %s', uid)
            yield False
            return
        logger.debug('Successfully authenticated card with UID %s', uid)
        yield True
        logger.debug('Stopping crypto with card with UID %s', uid)
        self._reader.MFRC522_StopCrypto1()

    def write_to_card(self, uid, sector_number, data):
        with self._select_tag_and_athenticate(uid, sector_number) as success:
            if not success:
                logger.error(
                    'Could not select or authenticate card with UID %s', uid)
                return None
            # unsure why this is done
            self._reader.MFRC522_Read(_trailer_block_for_sector(sector_number))
            for i in range(3):
                self._reader.MFRC522_Write(sector_number * 4 + i,
                                           data[i * 16:(i + 1) * 16])
            return data
