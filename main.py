# Mqtt Reader. Read RFID tags and broadcast UID via MQTT. This is educational software.
# Copyright (C) 2019 Marcus Gelderie
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import argparse
import asyncio
import contextlib
import logging
import time
import uuid

import mqtt
import rfid
import RPi

logger = logging.getLogger('RFID reader')

__VERSION = '???'


@contextlib.contextmanager
def gpio_cleaner():
    try:
        yield
    finally:
        RPi.GPIO.cleanup()


async def read_tag():
    reader = rfid.Reader()
    tag = reader.get_card()
    while not tag:
        await asyncio.sleep(0.3)
        tag = reader.get_card()
    return tag


def identity_from_tag(tag):
    logger.info('Processing tag %s', tag)
    return tag.uid()


class Dispatcher:
    def __init__(self, host, port, instance_uuid):
        self._mqtt = mqtt.MQTTClient()
        self._mqtt.connect(host, port)
        self._mqtt_loop = asyncio.create_task(self._mqtt.run())
        self._mqtt_responder_task = None
        self._setup_done = False
        self._uuid = instance_uuid
        self._response_topic = 'identity/response/{}'.format(
            self._uuid.hex.lower())

    async def done_with_setup(self):
        logging.debug('Waiting for connection to be ACKed')
        await self._mqtt.connected()
        logging.debug('Connection has been ACKed by broker')
        logging.debug('Subscribing to topic \'%s\'...', self._response_topic)
        message_id = self._mqtt.subscribe(self._response_topic)
        await self._mqtt.subscribed(message_id)
        logging.debug('Subscribed to topic \'%s\'', self._response_topic)
        self._mqtt_responder_task = asyncio.create_task(self._mqtt_responder())

    async def _mqtt_responder(self):
        while True:
            message = await self._mqtt.receive(self._response_topic)
            logger.info('Received response: %s', message.payload)

    async def dispatch(self, identity):
        message = bytearray(identity)
        message.extend(self._uuid.bytes)
        message_id = self._mqtt.publish('identity', message)
        await self._mqtt.published(message_id)

    async def shutdown(self):
        if self._mqtt_responder_task:
            await self._mqtt_responder_task.cancel()
        self._mqtt.cancel()
        # TODO: Maybe this should be on MQTTClient class
        await self._mqtt_loop


class Led():
    def __init__(self, number):
        self._number = number
        RPi.GPIO.setup(self._number, RPi.GPIO.OUT)
        RPi.GPIO.output(self._number, RPi.GPIO.LOW)

    def on(self):  # pylint: disable=invalid-name
        RPi.GPIO.output(self._number, RPi.GPIO.HIGH)

    def off(self):
        RPi.GPIO.output(self._number, RPi.GPIO.LOW)

    def on_for_seconds(self, duration):
        self.on()
        time.sleep(duration)
        self.off()

    async def async_on_for_seconds(self, duration):
        self.on()
        await asyncio.sleep(duration)
        self.off()

    def blink(self):
        self.on_for_seconds(0.1)

    def blink_times(self, times):
        for _ in range(times):
            self.blink()
            time.sleep(0.2)


async def main(host, port):
    this_uuid = uuid.uuid4()
    print('[+] This reader goes by the name of:', this_uuid.hex)
    dispatcher = Dispatcher(host, port, this_uuid)
    RPi.GPIO.setmode(RPi.GPIO.BCM)
    led = Led(16)

    await dispatcher.done_with_setup()
    logger.info('Dispatcher done.')
    print('[+] Waiting for tags')
    #blocks
    led.blink_times(5)
    while True:
        tag = await read_tag()
        led_task = asyncio.create_task(led.async_on_for_seconds(.7))
        print('[+] Got tag: {!s}'.format(tag))
        identity = identity_from_tag(tag)
        logger.info('Processing identity "%s"', identity)
        await dispatcher.dispatch(identity)
        # Allow for some time to remove the card
        await led_task


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--log-level',
                        '-L',
                        help='Log level',
                        choices=['INFO', 'DEBUG', 'WARN', 'ERROR'],
                        action='store',
                        default='INFO')
    parser.add_argument('host',
                        help='The broker hostname',
                        action='store',
                        default='localhost')
    parser.add_argument('port',
                        help='The broker port',
                        action='store',
                        default=1883,
                        type=int)
    parser.add_argument('--version', action='version', version=f'v{__VERSION}')
    return parser.parse_args()


if __name__ == "__main__":
    with gpio_cleaner():
        args = parse_arguments()
        logging.basicConfig(level=args.log_level)
        asyncio.run(main(args.host, args.port))
